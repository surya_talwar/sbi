var express = require('express')
var bodyParser = require('body-parser')
const app = new express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}))
// parse application/json
app.use(bodyParser.json())

const fs = require('fs');
var url = require('url');
var await = require('await');

const fetch = require('node-fetch');
const Bluebird = require('bluebird');
fetch.Promise = Bluebird;

const appPort = "9031"; // node JS application port
const bot_id = ["1751"]; // Bot ID
//global.base_url = "https://ai1.avaamo.com/runner_uat";

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey("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");
//const touser = ["manish.patil@sbimf.com", "ameyaghaisas@sbimf.com", "arjun.menon@sbimf.com","fredric@avaamo.com"];
const touser = ["suresh@avaamo.com","fredric@avaamo.com"];


app.get('/runner_get_report', (req, res) => {
    sendReport();
    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.write("Report mail sent successfully.");
    res.end();
})

const cron = require('node-cron');
// cron to start
var task = cron.schedule('00 09 * * *', () => {
    console.log('Printing this line every day at 09:00 Hours Indian Time.');
    sendReport();
}, {
    scheduled: true,
    timezone: "Asia/Kolkata"
});

// start method is called to start the above defined cron job
task.start();
console.log("cron is intiated.");

// tell the server what port to listen on
app.listen(appPort, () => {
    console.log('Listening on localhost:' + appPort)
})

// send report
async function sendReport() {
    let yesterdayDate = GetFormattedDate("T");
    try {
        // get the runner report
        var csvString = await getReport();
        var attach = [];
        var base64File = new Buffer.from(csvString).toString('base64');
        attach.push({
            content: base64File,
            filename: "Runner_" + yesterdayDate + '.csv'
        });

        // send mail
        const msg = {
            to: touser,
            from: 'reports@avaamo.com',
            subject: 'Unhandled Queries: ' + yesterdayDate,
            html: "PFA Unhandled report for " + yesterdayDate + ".",
            attachments: attach,
        };

        sgMail.send(msg, (error, result) => {
            if (error) {
                //Do something with the error
                //console.log(error);
                sendErrorReport(error.toString());
                return true;
            } else {
                console.log("Mail sent.");
                return true;
            }
        });

    } catch (error) {
        console.error("ERROR:" + error);
        sendErrorReport(error.toString());
    }
}

/* error report to the dev team */
function sendErrorReport(error) {
    let yesterdayDate = GetFormattedDate("T");
    const msg = {
        to: "abhinav@avaamo.com",
        from: 'sbi_error@avaamo.com',
        subject: 'SBI report error' + yesterdayDate,
        //text: 'and easy to do anywhere, even with Node.js',
        html: "Error reports. <br> " + error,
    };
    sgMail.send(msg, (error, result) => {
        if (error) {
            return true;
        } else {
            return true;
        }
    });
}


/* get log report ....*/

async function getReport() {
    let yesterdayDate = GetFormattedDate("T");
    // ===================================================FTD =============


    var FTD_UNHANDLED = {
        "bot_ids": bot_id,
        "intent_types": ["UNHANDLED::INTENT"],
        "start_date": yesterdayDate,
        "end_date": yesterdayDate,
        "intent_ids": [],
        "per_page" : "1000" // Will display a maximum of 1000 entries.
    };
    var FTD_UNHANDLED_Response = await fetchReport(FTD_UNHANDLED);
    var ftdstr = "";
    var created_date = new Date();
    for (let i = 0; i < FTD_UNHANDLED_Response.entries.length; ++i)
    {
        created_date = new Date(FTD_UNHANDLED_Response.entries[i].created_at * 1000);
        ftdstr += FTD_UNHANDLED_Response.entries[i].bot.display_name + "," + JSON.stringify(FTD_UNHANDLED_Response.entries[i].user_query) + "," + FTD_UNHANDLED_Response.entries[i].user.display_name + "," + FTD_UNHANDLED_Response.entries[i].user.id + "," + FTD_UNHANDLED_Response.entries[i].intent_type + "," + FTD_UNHANDLED_Response.entries[i].channel_name + "," + created_date.toString() + "\n";
    }
    console.log(ftdstr);

    var logString = "Bot Name, User Query, User name, User ID, Intent Type, Channel, Created at\n";
    logString += ftdstr;

    return logString;
}


async function fetchReport(params) {
    let options = {
        method: "POST",
        body: JSON.stringify(params),
        headers: {
            "Content-Type": "application/json",
            "Access-Token": "04012dd9c180414e902bd2ef2f7c89a3"
        }
    };
    return fetch("https://c3.avaamo.com/dashboard/bots_audience_lists/result_by_filters.json", options)
        .then((response) => {
            return response.json();
        }).then((responseData) => {
            return responseData;
        }).catch((e) => {
            console.log("API Exception =============>>>>> ", e);
        });
}

function GetFormattedDate(type) {
    var todayTime = new Date();
    if (type === "T") {
        var month = todayTime.getMonth() + 1;
        var yesterday = todayTime.getDate() - 1;
        var year = todayTime.getFullYear();
        return yesterday + "/" + month + "/" + year;
    } else if (type === "M") {
        var month = todayTime.getMonth() + 1;
        var year = todayTime.getFullYear();
        return "01" + "/" + month + "/" + year;
    } else if (type === "Y") {
        var year = todayTime.getFullYear();
        return "01/01/" + year;
    }
}
